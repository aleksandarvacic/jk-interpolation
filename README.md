# jk-interpolation

Interpolation functions for Quadratic, Cubic, Exponential and Sinusoidal easing.

By **Joris Kluivers**

- Read my weblog at [http://joris.kluivers.nl](http://joris.kluivers.nl)
- Follow [@kluivers on Twitter](http://twitter.com/kluivers)

## Usage

Each function has the signature of the form:

	:::objective-c
		CGFloat JKInterpolation(CGFloat  time, CGFloat start, CGFloat end);

- **time**: the interpolation progress in range of 0.0 to 1.0
- **start**: the initial value for time=0.0
- **end**: the resulting value for time=1.0

## Available functions

See `JKInterpolationMath.h`:

	:::objective-c
		/* Linear interpolation */
		CGFloat JKLinearInterpolation(CGFloat t, CGFloat start, CGFloat end);

		/* Quadric interpolation t^2 */
		CGFloat JKQuadraticInInterpolation(CGFloat t, CGFloat start, CGFloat end);
		CGFloat JKQuadraticOutInterpolation(CGFloat t, CGFloat start, CGFloat end);
		CGFloat JKQuadraticInOutInterpolation(CGFloat t, CGFloat start, CGFloat end);

		/* Cubic interpolation t^3 */
		CGFloat JKCubicInInterpolation(CGFloat t, CGFloat start, CGFloat end);
		CGFloat JKCubicOutInterpolation(CGFloat t, CGFloat start, CGFloat end);
		CGFloat JKCubicInOutInterpolation(CGFloat t, CGFloat start, CGFloat end);
		
		/* Exponential interpolation 2^t */
		CGFloat JKExponentialInInterpolation(CGFloat t, CGFloat start, CGFloat end);
		CGFloat JKExponentialOutInterpolation(CGFloat t, CGFloat start, CGFloat end);
		CGFloat JKExponentialInOutInterpolation(CGFloat t, CGFloat start, CGFloat end);
		
		/* Sinusoidal interpolation cos(t) / sin(t) */
		CGFloat JKSinusoidalInInterpolation(CGFloat t, CGFloat start, CGFloat end);
		CGFloat JKSinusoidalOutInterpolation(CGFloat t, CGFloat start, CGFloat end);
		CGFloat JKSinusoidalInOutInterpolation(CGFloat t, CGFloat start, CGFloat end);

## Generator project

The Generator utility generates a JSON output with interpolations for each of the functions, to be shown in a graph like:

[Animation Curves](http://joris.kluivers.nl/downloads/curves/) 
