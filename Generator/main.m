//
//  main.m
//  Generator
//
//  Created by Joris Kluivers on 5/26/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JKInterpolationMath.h"

static CGFloat (*interpolation[])(CGFloat, CGFloat, CGFloat) = {
    &JKLinearInterpolation,
    &JKQuadraticInInterpolation,
    &JKQuadraticOutInterpolation,
    &JKQuadraticInOutInterpolation,
    &JKCubicInInterpolation,
    &JKCubicOutInterpolation,
    &JKCubicInOutInterpolation,
    &JKExponentialInInterpolation,
    &JKExponentialOutInterpolation,
    &JKExponentialInOutInterpolation,
    &JKSinusoidalInInterpolation,
    &JKSinusoidalOutInterpolation,
    &JKSinusoidalInOutInterpolation
};

#define JK_INTERPOLATION_COUNT 13

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        NSArray *labels = @[
            @"Linear",
            @"Quadratic In",
            @"Quadratic Out",
            @"Quadratic In Out",
            @"Cubic In",
            @"Cubic Out",
            @"Cubic In Out",
            @"Exponential In",
            @"Exponential Out",
            @"Exponential In Out",
            @"Sinusoidal In",
            @"Sinusoidal Out",
            @"Sinusoidal In Out"
        ];
        
        NSMutableDictionary *data = [NSMutableDictionary dictionary];
        
        CGFloat (*interpolationFunc)(CGFloat, CGFloat, CGFloat) = NULL;
        
        for (int i=0; i<JK_INTERPOLATION_COUNT; i++) {
            interpolationFunc = interpolation[i];
            
            NSMutableArray *serieData = [NSMutableArray array];
            
            for (CGFloat j=0; j<=1.1f; j+= 0.05f) {
                [serieData addObject:@[@(j), @(interpolationFunc(j, 0.f, 1.0f))]];
            }
            
            [data setObject:serieData forKey:labels[i]];
        }
        
        [data setObject:labels forKey:@"_keyOrdering"];
        
        NSError *jsonError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:0 error:&jsonError];
        if (!jsonData) {
            NSLog(@"Error: %@", jsonError);
            return 0;
        }
        
        NSFileHandle *std = [NSFileHandle fileHandleWithStandardOutput];
        [std writeData:jsonData];
        
    }
    return 0;
}

